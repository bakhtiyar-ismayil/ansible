# 1. Users and Passwords
  
This playbook reads the usernames and passwords from the input files using the paste command, creates a user account for each username using the ansible.builtin.user module, and outputs the username and password for each user account created using the debug module. 

```
---
- name: Create user accounts
  hosts: all
  become: true
  
  vars:
    user_file: "usernames.txt"
    pass_file: "passwords.txt"

  tasks:
    - name: Read usernames and passwords from files
      shell: paste -d':' "{{ user_file }}" "{{ pass_file }}"
      register: users

    - name: Create user accounts
      ansible.builtin.user:
        name: "{{ item.split(':')[0] }}"
        password: "{{ item.split(':')[1] | password_hash('sha512') }}"
        createhome: yes
        shell: /bin/bash
      with_items: "{{ users.stdout_lines }}"

    - name: Output created users
      debug:
        msg: "User: {{ item.split(':')[0] }}, Password: {{ item.split(':')[1] }}"
      with_items: "{{ users.stdout_lines }}"
```
